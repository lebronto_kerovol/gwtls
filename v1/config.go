package v1

import (
	"crypto/tls"
	"encoding/json"
	"errors"

	"bitbucket.org/lebronto_kerovol/gwerror"
)

const (
	ERROR_EXCEPTION_ON_GET_CERTIFICAT_AND_KEY                        = `ERROR: Exception on - Get certificat and key.`
	ERROR_EXCEPTION_ON_GET_CERTIFICAT_AND_PUBLIC_KEY_AND_PRIVATE_KEY = `ERROR: Exception on - Get certificat, public key and private key.`
)

const (
	GWJWT_CONFIG_PATH_ROUTE = `gwtls/v1/config`
)

var (
	errorExceptionOnGetCertificatAndKey                    = gwerror.NewErrorFromString(``, GWJWT_CONFIG_PATH_ROUTE, ERROR_EXCEPTION_ON_GET_CERTIFICAT_AND_KEY)
	errorExceptionOnGetCertificatAndPublicKeyAndPrivateKey = gwerror.NewErrorFromString(``, GWJWT_CONFIG_PATH_ROUTE, ERROR_EXCEPTION_ON_GET_CERTIFICAT_AND_PUBLIC_KEY_AND_PRIVATE_KEY)
)

func getErrorExceptionOnGetCertificatAndKey() *gwerror.Error {
	return errorExceptionOnGetCertificatAndKey
}

func getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey() *gwerror.Error {
	return errorExceptionOnGetCertificatAndPublicKeyAndPrivateKey
}

type Config struct {
	CodeMethod     *string `json:"code-method,omitempty"`
	Certificat     *string `json:"certificat-pem,omitempty"`
	PrivateKey     *string `json:"private-key-pem,omitempty"`
	PublicKey      *string `json:"public-key-pem,omitempty"`
	CodeCertificat *string `json:"code-certificat,omitempty"`
}

const (
	JSON_CERTIFICAT  = `certificat`
	JSON_KEY         = "key"
	JSON_PRIVATE_KEY = `private_key`
	JSON_PUBLIC_KEY  = `public_key`
)

func getCertificatAndKey(dict *map[string]interface{}) (string, string, error) {
	fieldCertificat, ok := (*dict)[JSON_CERTIFICAT]
	if !ok {
		return ``, ``, errors.New(`ERROR: JSON field "certificat" is not exist.`)
	}
	certificat, ok := fieldCertificat.(string)
	if !ok {
		return ``, ``, errors.New(`ERROR: Type "certificat" is invalid.`)
	}

	fieldKey, ok := (*dict)[JSON_KEY]
	if !ok {
		return ``, ``, errors.New(`ERROR: JSON field "key" is not exist.`)
	}
	key, ok := fieldKey.(string)
	if !ok {
		return ``, ``, errors.New(`ERROR: Type "key" is invalid.`)
	}

	return certificat, key, nil
}

func getCertificatAndPublicKeyAndPrivateKeyFromDict(dict *map[string]interface{}) (string, string, string, error) {
	fieldCertificat, ok := (*dict)[JSON_CERTIFICAT]
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: JSON field "certificat" is not exist.`)
	}
	certificat, ok := fieldCertificat.(string)
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: Type "certificat" is invalid.`)
	}

	fieldPublicKey, ok := (*dict)[JSON_PUBLIC_KEY]
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: JSON field "public_key" is not exist.`)
	}
	publicKey, ok := fieldPublicKey.(string)
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: Type "public_key" is invalid.`)
	}

	fieldPrivateKey, ok := (*dict)[JSON_PRIVATE_KEY]
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: JSON field "private_key" is not exist.`)
	}
	privateKey, ok := fieldPrivateKey.(string)
	if !ok {
		return ``, ``, ``, errors.New(`ERROR: Type "private_key" is invalid.`)
	}

	return certificat, publicKey, privateKey, nil
}

func NewConfigFromJSON(codeAlgorithm string, sourceJson []byte, codeCertificat string) (*Config, error) {
	if codeAlgorithm == `` {
		return nil, gwerror.NewError(``, GWJWT_CONFIG_PATH_ROUTE, errors.New(`ERROR: Parameter "codeAlgorithm" is empty.`))
	}

	var dict map[string]interface{}
	if err := json.Unmarshal(sourceJson, &dict); err != nil {
		return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, errors.New(`ERROR: Parameter "sourceJson" is invalid.`), err)
	}

	switch codeAlgorithm {
	case CODE_METHOD_HS256:
		return nil, gwerror.NewErrorFromString(``, ``, `ERROR: TLS algorithm is not support.`)
		{
			certificat, key, err := getCertificatAndKey(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &key, PublicKey: &key, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_HS384:
		return nil, gwerror.NewErrorFromString(``, ``, `ERROR: TLS algorithm is not support.`)
		{
			certificat, key, err := getCertificatAndKey(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &key, PublicKey: &key, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_HS512:
		return nil, gwerror.NewErrorFromString(``, ``, `ERROR: TLS algorithm is not support.`)
		{
			certificat, key, err := getCertificatAndKey(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &key, PublicKey: &key, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_RS256:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_RS384:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_RS512:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_PS256:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_PS384:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_PS512:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_ES256:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_ES384:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	case CODE_METHOD_ES512:
		{
			certificat, publicKey, privateKey, err := getCertificatAndPublicKeyAndPrivateKeyFromDict(&dict)
			if err != nil {
				return nil, gwerror.WrapError(``, GWJWT_CONFIG_PATH_ROUTE, getErrorExceptionOnGetCertificatAndPublicKeyAndPrivateKey(), err)
			}

			return &Config{CodeMethod: &codeAlgorithm, Certificat: &certificat, PrivateKey: &privateKey, PublicKey: &publicKey, CodeCertificat: &codeCertificat}, nil
		}
	}

	return nil, gwerror.NewErrorFromString(``, ``, `ERROR: Code algorithm is not supported.`)
}

func (conf *Config) GetCodeCertificat() (string, error) {
	if conf == nil {
		return "", errors.New(`ERROR: TLS Config is nil`)
	}

	if conf.CodeCertificat == nil {
		return "", errors.New(`ERROR: Field "CodeCertificat" TLS Config is nil`)
	}

	return *conf.CodeCertificat, nil
}

func (conf *Config) GetCertificat() ([]byte, error) {
	if conf == nil {
		return nil, errors.New(`ERROR: TLS Config is nil`)
	}

	if conf.Certificat == nil {
		return nil, errors.New(`ERROR: Field "Certificat" TLS Config is nil`)
	}

	return []byte(*conf.Certificat), nil
}

func (conf *Config) GetPrivateKey() ([]byte, error) {
	if conf == nil {
		return nil, errors.New(`ERROR: TLS Config is nil`)
	}

	if conf.PrivateKey == nil {
		return nil, errors.New(`ERROR: Field "PrivateKey" TLS Config is nil`)
	}

	return []byte(*conf.PrivateKey), nil
}

func (conf *Config) GetPublicKey() ([]byte, error) {
	if conf == nil {
		return nil, errors.New(`ERROR: TLS Config is nil`)
	}

	if conf.PublicKey == nil {
		return nil, errors.New(`ERROR: Field "PublicKey" TLS Config is nil`)
	}

	return []byte(*conf.PublicKey), nil
}

func (conf *Config) GetTLSConfig() (*tls.Config, error) {
	certPem, err := conf.GetCertificat()
	if err != nil {
		return nil, err
	}

	keyPem, err := conf.GetPrivateKey()
	if err != nil {
		return nil, err
	}

	cert, err := tls.X509KeyPair(certPem, keyPem)
	if err != nil {
		return nil, err
	}

	return &tls.Config{Certificates: []tls.Certificate{cert}}, nil
}
